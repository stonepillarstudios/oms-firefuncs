const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);
let db = admin.firestore();

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

//on every new vote, update the votes count
exports.upVote = functions.firestore
    .document('voting/{ballot}/votes/{voter}')
    .onCreate((snap, context) => {

        const topicRef = db.collection('voting/'+context.params.ballot+'/topics')
            .doc(snap.data().votee);

        return db.runTransaction(trans => {
            return trans.get(topicRef)
                .then(oldDoc=>{
                    return trans.update(topicRef, {votes: admin.firestore.FieldValue.increment(1)});
                });
        });
    });

// on vote deletion, update votes count
exports.downVote = functions.firestore
    .document('voting/{ballot}/votes/{voter}')
    .onDelete((snap, context) => {

        const topicRef = db.collection('voting/'+context.params.ballot+'/topics')
            .doc(snap.data().votee);

        return db.runTransaction(trans => {
            return trans.get(topicRef)
                .then(oldDoc=>{
                    return trans.update(topicRef, {votes: admin.firestore.FieldValue.increment(-1)});
                });
        });
    });

// maintain totals as topics are added and deleted.
exports.topicAggs = functions.firestore
    .document('topics/{topic}')
    .onWrite((change, context) => {
        if (!change.before.exists){ // new doc
            return db.runTransaction(trans => {
                return new Promise(resolve => {resolve(trans.update(db.doc('aggs/topics'),
                    {total: admin.firestore.FieldValue.increment(1)}))});
            });
        }else if (!change.after.exists){ // del doc
            return db.runTransaction(trans => {
                return new Promise(resolve => {resolve(trans.update(db.doc('aggs/topics'),
                    {total: admin.firestore.FieldValue.increment(-1)}))});
            });
        }
        return null;
    });
